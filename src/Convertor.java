

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * Utility for converting ResultSets into some Output formats
 */
public class Convertor {
    public static final String CHILDREN = "\"children\": [\n ";
    public static final String NAME = "\"name\": ";
    public static final String SIZE = "\"size\": ";


     public static void writeOpenGroup(FileWriter f, String blockName) throws IOException {
            f.write("{\n");

                f.write(NAME + "\"" + blockName + "\",");


            f.write(CHILDREN);

     }

     public static void writeCloseGroup(FileWriter f) throws IOException {
         f.write("]\n},\n");
     }

    public static void writeEndCloseGroup(FileWriter f) throws IOException {
        f.write("]\n}\n");
    }



}