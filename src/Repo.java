import org.kohsuke.github.*;

import java.io.IOException;
import java.util.Date;

public class Repo {
    GHUser account;
    GitHub gitHub;
    GHRepository repo;
    public Repo(GHUser account, GitHub gitHub){
        this.account=account;
        this.gitHub=gitHub;
    }

    public GHRepository createRepo(String repoName) throws IOException {

        GHCreateRepositoryBuilder temp= gitHub.createRepository(repoName);
        temp.autoInit(true);
        GHRepository repo=temp.create();
        this.repo=repo;
        return repo;


    }

    public void deleteRepo() throws IOException {
        this.repo.delete();

    }


}
