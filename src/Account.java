import org.kohsuke.github.*;

import java.io.IOException;
import java.util.*;


public class Account {
    GHUser account;
    public Account(GHUser account) throws IOException {


         this.account= account;
    }
    public void basicDetails() throws IOException {
        System.out.println("Personal Details");
        String name=account.getName();
        System.out.println("name:"+ name+", login:"+ account.getLogin()+", email:"+ account.getEmail());
    }

    public void basicStats() throws IOException {
        String name=account.getName();
        int numberRepos;
        Map<String , GHRepository> map= account.getRepositories();
        numberRepos=map.size();
        int i=0;
        int totalSize=0; int totalNumberCommits=0;
        int sizeRepo=0, numberCommits;
        String repoStatus;
        System.out.println(name+" has the following "+numberRepos+" repos");
        Collection<GHRepository> collection=map.values();
        Iterator<GHRepository> iterator=collection.iterator();
        while(iterator.hasNext()){
            GHRepository temp=iterator.next();
            sizeRepo = temp.getSize();
                if(sizeRepo>0) {
                    PagedIterable commits = temp.listCommits();
                    if (temp.isPrivate()) {
                        repoStatus = "Private";
                    } else {
                        repoStatus = "Public";
                    }

                    totalSize += sizeRepo;
                    numberCommits = commits.asList().size();
                    totalNumberCommits += numberCommits;
                    i++;
                    System.out.println(repoStatus + " repo name:" + temp.getName() + ", size in KB:" + sizeRepo + ", number commits:" + numberCommits);
                }


        }
        if(i>0) {
            if(i!=sizeRepo){
                System.out.println("This account has a further"+(numberRepos-i)+" repos that are not included here as they are empty");
            }
            System.out.println("average repo size of " + totalSize/i+" kilo bytes");
            System.out.println("average number of commits "+totalNumberCommits/i);
        }
        else

            System.out.println("Account has no repos");
    }
    private List<GHUser> listFollowers() throws IOException {
        String theirName=account.getName();
        System.out.println(theirName+" has "+ account.getFollowersCount()+" followers who are");
       PagedIterable<GHUser>p= account.listFollowers();
       List<GHUser> list=p.asList();
        GHUser follower;
        String name;
       for(int i=0;i<list.size();i++){
           follower=list.get(i);
           name=follower.getName();
           if(name==null) {
               System.out.print("name:User has not set a public name, ");
           }
           else {
               System.out.print("name:"+name+" ,");
           }
           System.out.println("login:"+follower.getLogin());
       }
       return  list;

    }



    public List<GHUser> overview() throws  IOException{
        basicDetails();
        basicStats();
        List<GHUser> listFollowers=listFollowers();
        return listFollowers;

    }

}
