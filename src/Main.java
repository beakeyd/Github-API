
//The code for connecting to the database and creating a table were taken from
//https://www.tutorialspoint.com/jdbc/jdbc-sample-code.htm
//STEP 1. Import required packages
import org.kohsuke.github.*;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.stream.IntStream;

public class Main {
    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/organization";

    //  Database credentials
    static final String USER = "newuser";
    static final String PASS = "password";
    public static final String DELIM = ",";
    public static final String NEW_LINE = "\n";
    public static final String CHILDREN = "\"children\": [\n ";
    public static final String NAME = "\"name\": ";
    public static final String OPEN_BRACKET ="{\n ";
    public static final String CLOSED_BRACKET ="}\n";


    public static void main(String[] args) throws IOException {
        Connection conn = null;
        Statement stmt = null;
        System.out.println("sdg");

        GitHub github=GitHub.connect();
        System.out.println("sdg");
        try{
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();

            String[] tableArray={"googlechrome","facebookresearch", "plataformatec", "deepmind", "ReactTraining"};



            for(int index=0;index<tableArray.length;index++) {

                String sql = "Create Table "+tableArray[index]+"\n" +
                        "(\n" +
                        "user_name VARCHAR(255),\n" +
                        "repo_name VARCHAR(255),\n" +
                        "count INT(10),\n" +
                        "Primary Key (user_name, repo_name))";
                stmt.executeUpdate(sql);

                System.out.println("Table Inserted successfully");
                String organName=tableArray[index];
                if(tableArray[index].contains("_")){
                    organName=organName.replace("_", "-");
                }
                GHOrganization organ = github.getOrganization(organName);
                PagedIterable<GHRepository> repoAsIterable = organ.listRepositories(100);
                List<GHRepository> repoAsList = repoAsIterable.asList();
                GHRepository thisRepo;
                int g = 0;
                for (int i = 0; i < repoAsList.size(); i++) {
                    thisRepo = repoAsList.get(i);
                    if (thisRepo.getSize() != 0) {
                        String repoName = thisRepo.getName();
                        PagedIterable<GHCommit> commitsAsIterable = thisRepo.listCommits();

                        List<GHCommit> commitsAsList = commitsAsIterable.asList();
                        GHCommit tempCommit;
                        String login;
                        int count;
                        GHUser user;

                        Map<String, Integer> repoMap = new HashMap<>();
                        for (int j = 0; j < commitsAsList.size(); j++) {
                            tempCommit = commitsAsList.get(j);
                            user = tempCommit.getAuthor();

                            if (user != null) {
                                login = user.getLogin();
                                //should be better way of doing this
                                if (repoMap.containsKey(login)) {
                                    count = repoMap.get(login);
                                    count++;
                                    repoMap.replace(login, count);
                                } else {
                                    repoMap.put(login, 1);
                                }
                            }

                        }


                        Iterator<String> iter = repoMap.keySet().iterator();

                        while (iter.hasNext()) {
                            login = iter.next();
                            count = repoMap.get(login);
                            sql = "INSERT INTO "+tableArray[index]+"(user_name, repo_name, count) VALUES ('" + login + "','" + repoName + "', '" + count + "') ON DUPLICATE KEY UPDATE count=count";
                            stmt.executeUpdate(sql);
                        }
                        System.out.println(g);
                        g++;
                    }

                }







            }
            

            Convertor con = new Convertor();
            for(int index=0;index<tableArray.length;index++) {

                FileWriter fileWriter = new FileWriter(tableArray[index]+".json");



                con.writeOpenGroup(fileWriter, tableArray[index]);
                Statement stmt2 = conn.createStatement();
                ResultSet list = stmt2.executeQuery("SELECT *\n" +
                        "FROM organization." + tableArray[index] + "\n" +
                        "ORDER BY repo_name;");
                list.next();

                String name, repoName, currentRepo;
                int count, level, s;
                name = list.getString("user_name");
                //System.out.println(name);
                repoName = list.getString("repo_name");

                currentRepo = repoName;
                count = list.getInt("count");
                level = count % 1000;
                if (level > 10)
                    level = 10;

                con.writeOpenGroup(fileWriter, repoName);
                fileWriter.append("{" + NAME + "\"" + name + "\"" + DELIM + " \"size\": " + count + "}");
                int i=0;
                while (list.next()) {
                    repoName = list.getString("repo_name");
                    name = list.getString("user_name");
                    count = list.getInt("count");
                    if (!repoName.equals(currentRepo)) {
                        con.writeCloseGroup(fileWriter);

                        con.writeOpenGroup(fileWriter, repoName);
                        currentRepo = repoName;
                        fileWriter.append("{" + NAME + "\"" + name + "\"" + DELIM + " \"size\": " + count + "}");
                    } else
                        fileWriter.append(",\n{" + NAME + "\"" + name + "\"" + DELIM + " \"size\": " + count + "}");


                }

                con.writeEndCloseGroup(fileWriter);
                con.writeEndCloseGroup(fileWriter);

                System.out.println("JSON file was created successfully !!!");
                fileWriter.flush();

                fileWriter.close();
            }


        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try


        }//end try

        // System.out.println("Goodbye!"+listRepos.asList().size());
    }//end main
}//end JDBCExample
